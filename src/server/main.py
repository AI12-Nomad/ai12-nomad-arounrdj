from server import ComController
from server.data.data_server_controller import DataServerController


def main():

    data_server_controller = DataServerController()
    com_server_controller = ComController()
    com_server_controller.set_interface_from_data(
        data_server_controller.get_my_interface_from_comm_server()
    )

    # initialiser des listeners com
    com_server_controller.init_server()

    # start pygame ihm
    # com_server_controller.start_ihm() # pour la V2


if __name__ == "__main__":
    main()
